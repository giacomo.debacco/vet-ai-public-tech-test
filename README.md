# VetAI Technical

We prepared an app for you. It's quite simple, composed of just 2 screens.
The first screen displays a list of Pokemon from this API: https://pokeapi.co/api/v2/pokemon/
The second screen displays the details of the pokemon selected.

The app is purposely buggy and could use some improvements.

You are to improve the app in any way you see fit and will need to explain the choices you make in the 2nd stage interview.

Feel free to choose any libraries you want to create your solution.

We suggest you spend a few hours on it. When you are satisfied with the result you can share the repo with us on Github to `GiacomoDB` or Gitlab to `giacomo.debacco`.
Any issue or question drop us an email at giacomo.de.bacco@vet-ai.com.

Please fork or download this repo and work on it on your own space. You can send it as instructed above.
Thank you.
